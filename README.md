# Advent Of Code

Here I track my solution, most of the time quick and dirty, to [Advent Of Code](https://adventofcode.com/)

They are separate by year and by day, e.g. [2023/01](2023/01/)

Usually I separate first and second part of each day puzzle, even if the code can be heavily shared

Alternate solution are provided with suffix like [2022/01 with chatgpt](2022/01-chatgpt/)

This has been started in November 2023, having a bit of fun with 2022 puzzle too while waiting for December 2023
