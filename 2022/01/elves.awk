
BEGIN {
	i=1
	elves[i] = 0
}

/^\s*$/ {
	++i
	elves[i] = 0;
}

/[-.0-9]*/ {
	elves[i] = elves[i] + $0
}

END {
	max_elves = 0
	max_cal = 0
	for (idx in elves) {
		print(elves[idx] " from elves " idx)
		if (elves[idx] > max_cal) {
			max_elves = idx
			max_cal = elves[idx]
			# print ("new max is " max_cal " by " max_elves)
		}
	}
	#print ("max_cal is " max_cal " by elves numer " max_elves)
}
