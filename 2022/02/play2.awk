
BEGIN {
	sum = 0
}

// {
	other=$1
	me=$2
	print other me

	# choose the right value by overwriting our choice
	if (other=="A") {
		switch (me) {
			case "X":
				me = "Z"
				break
			case "Y":
				me = "X"
				break
			case "Z":
				me = "Y"
				break
		}
	}
	if (other=="B") {
		switch (me) {
			case "X":
				me = "X"
				break
			case "Y":
				me = "Y"
				break
			case "Z":
				me = "Z"
				break
		}
	}
	if (other=="C") {
		switch (me) {
			case "X":
				me = "Y"
				break
			case "Y":
				me = "Z"
				break
			case "Z":
				me = "X"
				break
		}
	}

	switch (me) {
		case "X":
			turn = 1
			break
		case "Y":
			turn = 2
			break
		case "Z":
			turn = 3
			break
	}

	if (other=="A") {
		switch (me) {
			case "X":
				turn += 3
				break
			case "Y":
				turn += 6
				break
			case "Z":
				turn += 0
				break
		}
	}
	if (other=="B") {
		switch (me) {
			case "X":
				turn += 0
				break
			case "Y":
				turn += 3
				break
			case "Z":
				turn += 6
				break
		}
	}
	if (other=="C") {
		switch (me) {
			case "X":
				turn += 6
				break
			case "Y":
				turn += 0
				break
			case "Z":
				turn += 3
				break
		}
	}
	sum += turn
}

END {
	print ("game ends with " sum)
}

