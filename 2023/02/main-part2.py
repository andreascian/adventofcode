
import sys

input_data = sys.stdin.readlines()

total = 0
for line in input_data:
  max_cubes = {
    'red': 0,
    'green': 0,
    'blue': 0
  }

  (game, data) = line.split(":")
  game_nr = int(game.split(' ')[1])
  sets = data.split(";")
  possible = True
  for s in sets:
    cubes = s.split(",")
    for c in cubes:
      (nr, color) = c.strip().split(" ")
      if int(nr) > max_cubes[color]:
        max_cubes[color] = int(nr)

  power = 1
  for v in max_cubes.values():
    power = power * v

  total += power

print(total)
