
import sys

input_data = sys.stdin.readlines()

max_cubes = {
  'red': 12,
  'green': 13,
  'blue': 14
}

total = 0
for line in input_data:
  (game, data) = line.split(":")
  game_nr = int(game.split(' ')[1])
  sets = data.split(";")
  possible = True
  for s in sets:
    cubes = s.split(",")
    for c in cubes:
      (nr, color) = c.strip().split(" ")
      if int(nr) > max_cubes[color]:
        possible = False
  if possible:
    print(f"game {game_nr} is possible")
    total += game_nr
  else:
    print(f"game {game_nr} is IMPOSSIBLE")

print(total)
