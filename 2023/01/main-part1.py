
import sys
import re

input_data = sys.stdin.readlines()

data = []
numbers = []
total = 0
p = re.compile("\d")
for id in input_data:
  d = re.findall("\d", id)
  data.append(d)
  value = int(d[0] + d[-1])
  numbers.append(value)
  total += value

print (total)
