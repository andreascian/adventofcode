
import sys
import re

words_to_numbers = {
    'one': '1',
    'two': '2',
    'three': '3',
    'four': '4',
    'five': '5',
    'six': '6',
    'seven': '7',
    'eight': '8',
    'nine': '9',
}

input_data = sys.stdin.readlines()

converted = []
data = []
numbers = []
total = 0
text_to_number_re  = re.compile(r'|'.join(words_to_numbers.keys()))
digit_or_number  = re.compile(r'\d|' + '|'.join(words_to_numbers.keys()))
last_digit_or_number = re.compile(r'(.*)(' + r'\d|' + '|'.join(words_to_numbers.keys()) + ')')
for id_1 in input_data:
  # id = re.sub(text_to_number_re, lambda x: words_to_numbers[x.group()], id_1, 1)
  part1 = re.findall(digit_or_number, id_1)[0]
  print (part1)
  #id = re.sub(last_text_to_number_re, lambda x: words_to_numbers[x.group()], id, 1)
  part2 = re.findall(last_digit_or_number, id_1)[0][1]
  print (part2)
  part1 = re.sub(text_to_number_re, lambda x: words_to_numbers[x.group()], part1)
  part2 = re.sub(text_to_number_re, lambda x: words_to_numbers[x.group()], part2)
  value = int(part1 + part2)
  numbers.append(value)
  total += value

print (total)
