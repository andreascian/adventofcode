
import sys
import re
import numpy as np

input_data = sys.stdin.readlines()

# initialize base data
# how many cards we have
total_scartchards = len(input_data)

# assume that we have 1 copy of them at beginning
scratchcards = np.full(len(input_data), 1).tolist()
# save winning numbers
scratchcards_win = np.full(len(input_data), 0).tolist()

# extract data and calculate winning numbers
for (scratchcard_nr, line) in enumerate(input_data):
  (card, numbers) = line.split(":")
  (winning, having) = numbers.split("|")

  winning_nrs = winning.split()
  having_nrs = having.split()

  print(f"card {card} winning {winning_nrs} has {having_nrs}")

  win = 0
  winners = []
  for having_nr in having_nrs:
    if having_nr in winning_nrs:
      winners.append(having_nr)
      win += 1
  if win > 0:
    print(f"winners are {winners} (so {win} of them) with a total of {2**(win-1)}")
    scratchcards_win[scratchcard_nr] = win
  else:
    print("no winner :-(")

# now read all the card again and calculate how many of the we're winning
for (i, card_cnt) in enumerate(scratchcards):
  win = scratchcards_win[i]
  for cnt in range(win):
      scratchcards[i+cnt+1] += card_cnt

print(f"total scratchcards are {scratchcards}")

total = 0
for card_cnt in scratchcards:
  total += card_cnt

print(total)
