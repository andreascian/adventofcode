
import sys
import re

input_data = sys.stdin.readlines()

total = 0
for line in input_data:

  # simple split operation are enough for parsing data

  (card, numbers) = line.split(":")
  (winning, having) = numbers.split("|")

  # last split to convert useful data in array
  winning_nrs = winning.split()
  having_nrs = having.split()

  print(f"card {card} winning {winning_nrs} has {having_nrs}")

  win = 0
  winners = []
  for having_nr in having_nrs:
    if having_nr in winning_nrs:
      winners.append(having_nr)
      win += 1
  if win > 0:
    print(f"winners are {winners} (so {win} of them) with a total of {2**(win-1)}")
    total += (2**(win-1))
  else:
    print("no winner for this card :-(")



print(f"{total}")
