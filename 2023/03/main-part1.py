
import re
import sys

input_data = sys.stdin.readlines()

number_pattern = r"\d+"
symbol_pattern = r"[^\d\.\s]"

symbol_indexes = []


def has_symbol_adiacent(match, line_nr, symbols):
  for offset in (-1, 0, 1):

    try:
      # print(f"processing offset {offset}")
      for line_symbol in symbols[line_nr + offset]:
        if (line_symbol.start() >= match.start()-1 and line_symbol.start() <= match.end()):
          print(f"got it! {line_symbol.group()} is nearby {match.group()}")
          return True
    except IndexError:
      # print(f"failed offset {offset}")
      pass

  # if we're here, we didn't find a valid symbol near us
  return False

# collect symbols information
for line in input_data:
  line_symbols = []
  for match in re.finditer(symbol_pattern, line):
    # print(f"symbol {match.group()} start index {match.start()} End index {match.end()}")
    line_symbols.append(match)
  symbol_indexes.append(line_symbols)

# now look for parts
line_nr = 0
total = 0
for line in input_data:
  for match in re.finditer(number_pattern, line):
    # print(f"{match.group()} start index {match.start()} End index {match.end()}")
    if has_symbol_adiacent(match, line_nr, symbol_indexes):
      total += int(match.group())

  line_nr += 1

print(total)